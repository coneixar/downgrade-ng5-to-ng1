/**
 * This file defines the root module of the Angular 1 of the application.
 */

import {
  getAngularJSGlobal, getAngularLib, setAngularJSGlobal, setAngularLib,
  downgradeComponent, downgradeInjectable, downgradeModule, UpgradeComponent, UpgradeModule
} from '@angular/upgrade/static';
import * as angular from 'angular';

/* ================== *
 *  ANGULAR 1 MODULE  *
 * ================== */
import { Ng1AppModule } from 'ng1/app/app.module';


/* ================================= *
 *  ANGULAR 5 COMPONENTS DOWNGRADED  *
 * ================================= */

// Angular 5 component needed to be downgraded to be used inside Angular 1
import { Ng5Component } from 'ng5/components/ng5component/ng5component.component';
// Register Angular 5 Component to be used in Angular 1 module
angular
    .module(Ng1AppModule.name)
    .directive('ng5Component', <any>downgradeComponent({ component: Ng5Component }));

// Angular 5 service to be downgraded to be used inside Angular 1
import { Ng5Service } from 'ng5/services/ng5service.service';
// Register Angular 5 Service as a factory in Angular 1
angular
    .module(Ng1AppModule.name)
    .factory('ng5Service', <any>downgradeInjectable(Ng5Service));

// Export Angular 1 with downgraded Angular 5 components
export const Ng1App = Ng1AppModule;
