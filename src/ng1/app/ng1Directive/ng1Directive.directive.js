(function () {
  "use strict";
  const template = require("./ng1Directive.html");

  angular.module("app")
    .directive("ng1Directive", [function () {
      return {
        restrict: "E",
        replace: true,
        transclude: false,
        scope: {
          // ...
        },
        template: template,
        controllerAs: "ng1DirectiveCtrl",
        controller: [ '$scope', 'ng5Service', function ($scope, ng5Service) {
          var ng1DirectiveCtrl = this;

          ng1DirectiveCtrl.title = ng5Service.title;
        }]
      };
    }]);

})();
