var appModule = angular.module("app", []);

require("./ng1Directive");

module.exports = {
  Ng1AppModule: appModule
};
