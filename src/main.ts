import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { setUpLocationSync } from '@angular/router/upgrade';
import { setAngularLib } from '@angular/upgrade/static';
import * as angular from 'angular';

declare global {
  interface Window {
    angular: any;
  }
}
window.angular = angular;

import { environment } from './environments/environment';

import { NgxAppModule } from 'ng5/ngx_app';

if (environment.production) {
  enableProdMode();
}

setAngularLib(angular);

platformBrowserDynamic().bootstrapModule(NgxAppModule);

/**
 * We bootstrap the Angular 5 module first, and then, once it's done,
 * bootstrap the Angular 1 module.
 */
/*
platformBrowserDynamic().bootstrapModule(NgxAppModule).then(ref => {
  const upgrade = (<any>ref.instance).upgrade;
  // bootstrap angular1
  upgrade.bootstrap(document.body, [ Ng1AppModule.name ], { strictDi: true });
  setUpLocationSync(upgrade);
});
*/
