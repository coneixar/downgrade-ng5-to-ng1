/**
 * This file defines the root module of the Angular 5 of the application.
 */

// import angular5
import { NgModule, Component, AfterViewInit } from '@angular/core';
import { RouterModule, UrlHandlingStrategy } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { downgradeComponent, UpgradeModule } from '@angular/upgrade/static';
import { RouterUpgradeInitializer } from '@angular/router/upgrade';

import { Ng5App } from 'ng5/components/ng5app/ng5app.component';

import { Ng5Component } from 'ng5/components/ng5component/ng5component.component';
import { Ng5Service } from 'ng5/services/ng5service.service';

// This URL handling strategy is custom and application-specific.
// Using it we can tell the Angular 5 router to handle only URL starting with settings.
export class Ng1NgxUrlHandlingStrategy implements UrlHandlingStrategy {
  shouldProcessUrl(url) { return false; }
  extract(url) { return url; }
  merge(url, whole) { return url; }
}

@NgModule({
  imports: [
    BrowserModule,
    UpgradeModule,

    // We don't need to provide any routes.
    // The router will collect all routes from all the registered modules.
    RouterModule.forRoot([])
  ],
  providers: [ Ng5Service ],
  exports: [ Ng5App, Ng5Component ],
  bootstrap: [ Ng5App ],
  declarations: [ Ng5App, Ng5Component ],
  entryComponents: [
    Ng5Component
  ]
})
export class NgxAppModule {
}
