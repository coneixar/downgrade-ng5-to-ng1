import { TestBed, inject } from '@angular/core/testing';

import { Ng5Service } from './ng5service.service';

describe('Ng5Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Ng5serviceService]
    });
  });

  it('should be created', inject([Ng5Service], (service: Ng5Service) => {
    expect(service).toBeTruthy();
  }));
});
