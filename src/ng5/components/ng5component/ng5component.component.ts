import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ng5-component',
  templateUrl: './ng5component.component.html',
  styleUrls: ['./ng5component.component.scss']
})
export class Ng5Component {
  constructor(){
    console.log("========================= ng5 component to be downgraded constructor");
  }
}
