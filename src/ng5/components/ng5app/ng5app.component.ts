import { Component, OnInit, AfterViewInit } from '@angular/core';
import { downgradeComponent, UpgradeModule } from '@angular/upgrade/static';
import * as angular from 'angular';

import { Ng1App } from '../../../ng1/ng1_app';

@Component({
  selector: 'ng5-app',
  templateUrl: './ng5app.component.html',
  styleUrls: ['./ng5app.component.scss']
})
export class Ng5App implements AfterViewInit {
  constructor(public upgrade: UpgradeModule){
    console.log("========================= ng5 boostrap constructor");
  }
  ngAfterViewInit() {
    console.log("========================= ng5 boostrap");
    this.upgrade.bootstrap(document.body, [Ng1App.name], { strictDi: true });
  }
}
