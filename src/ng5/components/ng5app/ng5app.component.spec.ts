import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ng5AppComponent } from './ng5app.component';

describe('Ng5AppComponent', () => {
  let component: Ng5AppComponent;
  let fixture: ComponentFixture<Ng5AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ng5AppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ng5AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
